#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;
#define M 5

void PrintMatr(int a[][M], int n, int m);
void CreateMatr(int a[][M], int n, int m);

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));

    //n-COlS,m-ROWS
    const int n = 4;
    const int m = M;
    float SerArefCols[] = { 0 }, SerArefRows[] = { 0 };

    int a[n][m];

    CreateMatr(a, n, m);
    PrintMatr(a, n, m);

    return 0;
}

void CreateMatr(int a[][M], int n, int m)
{
    int i, j;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
            a[i][j] = rand() % 100 - 40;
    }
}

void PrintMatr(int a[][M], int n, int m)
{
    int i, j;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++) {
            cout << a[i][j] << "\t";
        }
        cout << endl;
    }
}


