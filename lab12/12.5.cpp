#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

void CreateMas(int Mas[], int size)
{
    for (int i = 0; i < size; i++)
    {
        Mas[i] = rand() % 20;
        cout << Mas[i] << " ";
    }
}

void Max(int Mas[], int size, int* ma)
{
    int i;
    *ma = Mas[0];
    for (i = 0; i < size; i++)
    {
        if (*ma < Mas[i])
        {
            *ma = Mas[i];
        }
    }
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));

    int const SIZE = 5;
    int Mas1[SIZE], Mas2[SIZE], Mas3[SIZE];
    int max;
    int kil;

    cout << "Mas1: ";
    CreateMas(Mas1, SIZE);
    cout << endl;
    Max(Mas1, SIZE, &max);
    cout << "Max: " << max << endl;

    cout << "Mas2: ";
    CreateMas(Mas2, SIZE);
    cout << endl;
    Max(Mas2, SIZE, &max);
    cout << "Max: " << max << endl;

    cout << "Mas3: ";
    CreateMas(Mas3, SIZE);
    cout << endl;
    Max(Mas3, SIZE, &max);
    cout << "Max: " << max << endl;


    return 0;
}

//cout << "Mas2: ";
//CreateMas(Mas2, SIZE);
//cout << endl;
//
//cout << "Mas3: ";
//CreateMas(Mas3, SIZE);
//cout << endl;




