#include <iostream>
#include <math.h>
using namespace std;

double sum(int fs, int fe, const double fn)
{
	double sm = 0;
	int i;
	i = fs - 1;
	do
	{
		i++;
		sm = sm + fn * abs(log10(i));
	} while (i <= fe);
	return sm;
}

double mul(int fs, int fe)
{
	double ml = 1;
	int i;
	i = fs - 1;
	do
	{
		i++;
		ml = ml * pow(cos(i), 2);
	} while (i <= fe);
	return ml;
}

int main()
{
	setlocale(LC_ALL, "ru");
	const double exp = 2.71, n=10, m=5, x=0.9;
	int is, ie, js, je;
	cout << "is = ";
	cin >> is;
	cout << "ie = ";
	cin >> ie;
	cout << "js = ";
	cin >> js;
	cout << "je = ";
	cin >> je;
	double s;
	s = pow(exp, -2 * x) * sum(is, ie, n) + mul(js, je);
	printf("s=%7.5f\n", s);
}