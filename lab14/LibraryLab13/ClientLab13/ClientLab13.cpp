﻿#include <iostream>
#include <Windows.h>
#include "LibraryLab13.h"

using namespace std;

void task1();
void task2();
void task3();
void task4();

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));

    cout << "TASK 1" << endl;
    task1();
    cout << endl << endl << "TASK 2" << endl;
    task2();
    cout << endl << endl << "TASK 3" << endl;
    task3();
    cout << endl << endl << "TASK 4" << endl;
    task4();
}

void task1()
{
    const int SIZE = 10;
    int a[SIZE];
    int max;
    int imax;

    cout << "Масив:" << endl;
    Mas(a, SIZE);

    cout << endl;
    cout << endl;

    Max(a, SIZE, &max, &imax);
    cout << "Max: " << max << endl;
    cout << "Index Max: " << imax << endl;
}

void task2()
{
    const int n = 2, m = 6, t = 4;
    int Mat[n][m][t];
    int i, j, k;

    cout << "Mat[2][6][4]: " << endl;
    Matrix(&Mat[0][0][0], n, m, t);

    cout << endl;

    int max;
    MatrixMax(&Mat[0][0][0], n, m, t, &max);
    cout << "Max: " << max << endl;
}

void task3()
{
    int const SIZE = 10;
    int a[SIZE];
    CreateA(a, SIZE);
    cout << "Масив: " << endl;
    PrintA(a, SIZE);
    cout << endl;
    Sort(a, SIZE);
}

void task4()
{
    const int n = 5, m = 5;
    int Mat[n][m];

    cout << "Matrix: " << endl;
    PrintMat(&Mat[0][0], n, m);

    int v[m] = { 0 };
    int suma[n] = { 0 };
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            v[j] = Mat[i][j];
        }
        suma[i] = sum(v, m);
    }
    cout << "Sum: " << endl;
    p_v(suma, n);
}