#include "pch.h"
#include <iostream>
#include "LibraryLab13.h"

using namespace std;

void Mas(int* a, int size) {
    for (int i = 0; i < size; i++)
    {
        a[i] = rand() % 20;
        cout << a[i] << " ";
    }
}

void Max(int* a, int size, int* ma, int* ia)
{
    int i;
    *ma = a[0];
    for (i = 1; i < size; i++)
    {
        if (*ma < a[i])
        {
            *ma = a[i];
            *ia = i;
        }
    }
}

void Matrix(int* Mat, int n, int m, int t)
{
    int i, j, k;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            for (k = 0; k < t; k++)
            {
                Mat[j + k * k + i * n * k] = rand() % 100;
                printf(" %4d", Mat[j + k * k + i * n * k]);
            }
            cout << endl;
        }
        cout << endl;
    }
}

void MatrixMax(int* Mat, int n, int m, int t, int* maxMat)
{
    int i, j, k;
    *maxMat = Mat[0];
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            for (k = 0; k < t; k++)
            {
                if (*maxMat < Mat[j + k * k + i * n * k])
                {
                    *maxMat = Mat[j + k * k + i * n * k];
                }
            }
        }
    }
}

void CreateA(int a[], int size)
{
    for (int i = 0; i < size; i++)
    {
        a[i] = rand() % 20;
    }
}

void PrintA(int a[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
}

void Sort(int a[], int size)
{
    int i, k, d;
    for (k = 1; k < size; k++)
    {
        for (i = 0; i < size - k; i++)
        {
            if (a[i] > a[i + 1])
            {
                d = a[i];
                a[i] = a[i + 1];
                a[i + 1] = d;
            }
        }
    }
    cout << "³����������� �����: " << endl;
    for (i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
}

void PrintMat(int* Matf, int nf, int mf)
{
    for (int i = 0; i < nf; i++)
    {
        for (int j = 0; j < mf; j++)
        {
            Matf[i * mf + j] = rand() % 20;
            printf("%5d", *(Matf + i * mf + j));
        }
        cout << endl;
    }
}

int p_v(int* vf, int k)
{
    for (int i = 0; i < k; i++)
    {
        printf("%5d", vf[i]);
    }
    return 0;
}

int sum(int* vf, int k)
{
    int i = 0, s = 0;
    for (i = 0; i < k; i++)
    {
        s = s + vf[i];
    }
    return s;
}