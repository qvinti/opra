#pragma once

#ifdef LIBRARYLAB13_EXPORTS
#define LIBRARYLAB13_API __declspec(dllexport)
#else
#define LIBRARYLAB13_API __declspec(dllimport)
#endif

extern "C" LIBRARYLAB13_API void Mas(int* a, int size);
extern "C" LIBRARYLAB13_API void Max(int* a, int size, int* ma, int* ia);

extern "C" LIBRARYLAB13_API void Matrix(int* Mat, int n, int m, int t);
extern "C" LIBRARYLAB13_API void MatrixMax(int* Mat, int n, int m, int t, int* maxMat);

extern "C" LIBRARYLAB13_API void CreateA(int a[], int size);
extern "C" LIBRARYLAB13_API void PrintA(int a[], int size);
extern "C" LIBRARYLAB13_API void Sort(int a[], int size);

extern "C" LIBRARYLAB13_API void PrintMat(int* Matf, int nf, int mf);
extern "C" LIBRARYLAB13_API int p_v(int* vf, int k);
extern "C" LIBRARYLAB13_API int sum(int* vf, int k);