#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <Windows.h>

using namespace std;    

void create_text_file(const char* path)
{
    fstream f1(path, ios::out);
    int n = 5, m = 6;
    f1 << n << " " << m << endl;
    // float** a = new float[n];
    for(int i=0; i<n; i++)
    {
        // a[i] = new float[m];
    	for(int j=0; j<m; j++)
    	{
    		f1 << rand()%100-40 << " ";
		}
		f1 << endl;
	}
	f1.close();
}

void create_binary(const char* path)
{
    fstream f1(path, ios::out | ios::binary);
    int n = 5, m = 6;
    f1.write((char*)&n, sizeof(n));
    f1.write((char*)&m, sizeof(m));
    for(int i=0; i<n; i++)
    {
    	for(int j=0; j<m; j++)
    	{
    	    float x=rand()%100-40;
    	    //cout << x << '\t';
    		f1.write((char*)&x, sizeof(x));
		}
 		//cout << endl;
	}
	f1.close();
}

void task1(const char* in_path, const char* out_path)
{
    fstream f1(in_path, ios::in);
    int n, m;
    f1 >> n >> m;
    float** a = new float*[n];
    for(int i=0; i<n; i++)
    {
        a[i] = new float[m];
    	for(int j=0; j<m; j++)
    	{
    		f1 >> a[i][j];
		}
	}
	
	float* b = new float[m];
	for (int j=0; j<m; j++)
	{
	    b[j]=0;
	    int k=0;
	    for (int i=0; i<n; i++)
	    {
	        if (a[i][j]<0)
	        {
	            b[j]+=a[i][j];
	            k++;
	        }
	    }
	    if (k>0)
	    	b[j]/=k;
	}
	
	FILE* f2 = freopen(out_path, "w+", stdout);
	for (int i=0; i<m; i++)
	    cout << b[i] << "\t";
	
	f1.close();
	fclose(f2);
}

void task2(const char* in_path, const char* out_path)
{
    fstream f1(in_path, ios::in | ios::binary);
    int n, m;
    f1.read((char*)&n, sizeof(n));
    f1.read((char*)&m, sizeof(m));
    float** a = new float*[n];
    for(int i=0; i<n; i++)
    {
        a[i] = new float[m];
    	f1.read((char*)a[i], m*sizeof(float));
	}
	
	float* b = new float[n];
	for (int i=0; i<n; i++)
	{
	    b[i]=0;
	    for (int j=0; j<m; j++)
	    {
	        b[i] += a[i][j];
	    }
	}
	
	FILE* f2 = freopen(out_path, "w+", stdout);
	for (int i=0; i<n; i++)
	    cout << b[i] << "\t";
	
	f1.close();
	fclose(f2);
}

void task3(const char* in_path, const char* out_path)
{
	fstream f1(in_path, ios::in);
	if (!f1.is_open())
	{
		cout << "error" << endl;
	}
	FILE *f2;
	f2=freopen(out_path, "w+", stdout);
	if (f2 == NULL)
		cout << "error" << endl;
	char str[100];
	string s1;
	string s2(" */-+");
	while (!f1.eof())
	{
		f1.getline(str, sizeof(str));
		s1=str;
		for (int i=0; i<(s1.size()); i++)
		{
			int p;
			p=s2.find(s1[i], 1);
			if (p>=0)
			{
				s1.erase(i, 1);
				i--;
			}
		}
		cout << s1 << endl;
	}
	fclose(f2);
	f1.close();
}


int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
  	//create_text_file("1.txt");
    //task1("1.txt", "2.txt");
    //create_binary("1.bin");
    //task2("1.bin", "3.txt");
    task3("text.txt", "retext.txt");
    return 0;
} 


