#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <Windows.h>
#include <ctime>
using namespace std;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int i, j, n, m;
	int* a;
	cout << "������ ��. ����� ������� = ";
	cin >> n;
	cout << "������ ��. �������� ������� = ";
	cin >> m;
	a = (int*)calloc(n * m, sizeof(int));
	if (!a)
	{
		cout << "������� ��� �������� ���'��." << endl;
		return 0;
	}
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			*(a + i * m + j) = rand() % 100 - 40;
		}
	}
	cout << "������� a[" << n << "x" << m << "]\n";
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			printf(" %3d", *(a + i * m + j));
		}
		cout << endl;
	}
	int max;
	for (i = 0; i < n; i++)
		for (j = i; j < m; j++)
			if (*(a + i * m + j) > max)
				max = *(a + i * m + j);

	for (i = 0; i < n; i++)
		for (j = i; i < m; i++)
			*(a + i * m + j) = max;

	cout << "������� a[" << n << "x" << m << "]\n";
	for ( i = 0; i < n; i++)
	{
		for ( j = 0; j < m; j++)
		{
			printf(" %3d", *(a + i * m + j));
		}
		cout << endl;
	}
	cout << endl;
	cout << "Max = " << max << endl;
	free(a);
	return 0;
}