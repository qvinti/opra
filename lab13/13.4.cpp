#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

void PrintMat(int* Matf, int nf, int mf);
int p_v(int* vf, int k);
int sum(int* vf, int k);

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));

    const int n = 5, m = 5;
    int Mat[n][m];

    cout << "Matrix: " << endl;
    PrintMat(&Mat[0][0], n, m);

    int v[] = { 0 };
    int suma[n] = { 0 };
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            v[j] = Mat[i][j];
        }
        suma[i] = sum(v, m);
    }
    cout << "Sum: " << endl;
    p_v(suma, n);

    return 0;
}

void PrintMat(int* Matf, int nf, int mf)
{
    for (int i = 0; i < nf; i++)
    {
        for (int j = 0; j < mf; j++)
        {
            Matf[i * mf + j] = rand() % 20;
            printf("%5d", *(Matf + i * mf + j));
        }
        cout << endl;
    }
}

int p_v(int* vf, int k)
{
    for (int i = 0; i < k; i++)
    {
        printf("%5d", vf[i]);
    }
    return 0;
}

int sum(int* vf, int k)
{
    int i = 0, s = 0;
    for (i = 0; i < k; i++)
    {
        s = s + vf[i];
    }
    return s;
}



