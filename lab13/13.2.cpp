#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

void Matrix(int* Mat, int n, int m, int t);
void MatrixMax(int* Mat, int n, int m, int t, int* maxMat);

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));

    const int n = 2, m = 6, t = 4;
    int Mat[n][m][t];
    int i, j, k;

    cout << "Mat[2][6][4]: " << endl;
    Matrix(&Mat[0][0][0], n, m, t);

    cout << endl;

    int max;
    MatrixMax(&Mat[0][0][0], n, m, t, &max);
    cout << "Max: " << max << endl;


    return 0;
}

void Matrix(int* Mat, int n, int m, int t)
{
    int i, j, k;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            for (k = 0; k < t; k++)
            {
                Mat[j + k * k + i * n * k] = rand() % 100;
                printf(" %4d", Mat[j + k * k + i * n * k]);
            }
            cout << endl;
        }
        cout << endl;
    }
}

void MatrixMax(int* Mat, int n, int m, int t, int* maxMat)
{
    int i, j, k;
    *maxMat = Mat[0];
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            for (k = 0; k < t; k++)
            {
                if (*maxMat < Mat[j + k * k + i * n * k])
                {
                    *maxMat = Mat[j + k * k + i * n * k];
                }
            }
        }
    }
}




