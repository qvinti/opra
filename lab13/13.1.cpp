#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

void Mas(int* a, int size);
void Max(int* a, int size, int* ma, int* ia);

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));

    const int SIZE = 10;
    int a[SIZE];
    int max;
    int imax;

    cout << "�����:" << endl;
    Mas(a, SIZE);

    cout << endl;
    cout << endl;

    Max(a, SIZE, &max, &imax);
    cout << "Max: " << max << endl;
    cout << "Index Max: " << imax << endl;

    return 0;
}

void Mas(int* a, int size) {
    for (int i = 0; i < size; i++)
    {
        a[i] = rand() % 20;
        cout << a[i] << " ";
    }
}

void Max(int* a, int size, int* ma, int* ia)
{
    int i;
    *ma = a[0];
    for (i = 1; i < size; i++)
    {
        if (*ma < a[i])
        {
            *ma = a[i];
            *ia = i;
        }
    }
}




