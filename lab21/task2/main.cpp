#include <windows.h>
#include <graphics.h>
#include <stdlib.h>
#include <iostream>
#include <conio.h>
#include <math.h>
using namespace std;
main()
{
	const int X=700,size=10;
	int i;
	float x,y,step=X/size;
	char lol[20],lol2[20];
	initwindow(X+step,X+step);
	setbkcolor(0);
	setcolor(15);
	outtextxy(X/2+5,X/2+3,"0");
	outtextxy(X/2+step,X/2+0.3*step," 1");
	outtextxy(X/2-step,X/2+0.3*step,"-1");
	outtextxy(X/2+0.3*step,X/2+step,"-1");
	outtextxy(X/2+0.3*step,X/2-step," 1");
	outtextxy(X-20,X/2+10,"X");
	outtextxy(X/2+10,10,"Y");
	line(0,X/2,X,X/2);
	line(X/2,0,X/2,X);
	line(X/2,0,X/2-6,6);
	line(X/2,0,X/2+6,6);
	line(X,X/2,X-6,X/2+6);
	line(X,X/2,X-6,X/2-6);
	for(i=1;i<size;i++){
		line(X/2-5,i*X/size,X/2+5,i*X/size);
	}
	for(i=0;i<size;i++){
		line(i*X/size,X/2-5,i*X/size,X/2+5);
	}
	
	setcolor(10);
	moveto(X/2, X/2);
	for(x=0;x<=size/2;x+=0.001){
		y=1/2+pow(sin(x),2); 
		lineto(X/2+x*step,X/2-y*step);
	}
	setcolor(3);
	outtextxy(40,20,"X");
	outtextxy(100,20,"Y");
	for(x=1,i=1;i<10;i++,x+=0.5){
		sprintf(lol,"%1.2f",x);
		sprintf(lol2,"%1.2f",1/2+pow(sin(x-1.0),2));
		outtextxy(25,20+i*20,lol);
		outtextxy(85,20+i*20,lol2);
	}
	setcolor(2);
	outtextxy(X/2+step*3,X/2-step*1.5,"y=1/2+sin^2(x)");
	getch();
	closegraph();
}
