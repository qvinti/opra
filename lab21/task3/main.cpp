#include <graphics.h>
#include <conio.h>

void gyro(int x, int y, int color)
{
	setcolor(color);
	setfillstyle (1,color); 
	bar(x,y,x+100,y+30);
	bar(x+20,y,x+40,y-20);
	line(x+40,y-20,x+100,y);
	bar(x+25,y-30,x+35,y-20);
	line(x-30,y-20,x+90,y-40);
	line(x-30,y-40,x+90,y-20);
	bar(x-15,y,x+15,y+15);
}

main()
{
    int i,x,y,z,v;
	x=30;
	y=200;
	v=1;
	initwindow(500, 500);
	for (i=1; i<300; i++)
	{  
	    gyro(x,y, 7);
		delay(5);
	    gyro(x,y,0);
	    x=x+v;
	}
	gyro(x,y, 7);
	getch();
	closegraph();
}
