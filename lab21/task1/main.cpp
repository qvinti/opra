#include <windows.h>
#include <graphics.h>
#include <stdlib.h>
#include <conio.h>
using namespace std;
main()
{
	const int X=700,a=200;
	char c1[20], c2[40]="V = 1/3 * So * H = ";
	initwindow(X,X);
	float V, S, H;
	H=a/10;
	S=(a^2)/10;
	V=(1.0/3.0)*S*H;
	sprintf(c1,"%5.2f",V);
	
	
	setusercharsize(5,5,5,5);
	setbkcolor(0);
	setcolor(15);
	rectangle(a,a,2*a,2*a);
	rectangle(a+60,a-60,(2*a)+60,(2*a)-60);
	moveto(a,a);
	linerel(60,-60);
	setlinestyle(1,0,0);
	linerel(70,230);
	setlinestyle(0,0,0);
	moveto(a*2,a);
	linerel(60,-60);
	setlinestyle(1,0,0);
	linerel(-130,230);
	setlinestyle(0,0,0);
	moveto(a,a*2);
	linerel(60,-60);
	moveto(a*2,a*2);
	linerel(60,-60);
	setlinestyle(1,0,0);
	moveto(a,a);
	linerel(260,-60);
	moveto(a,a*2);
	linerel(260,-60);
	moveto(a*2,a);
	linerel(-140, -60);
	moveto(a*2,a*2);
	linerel(-140, -60);
	linerel(140/2, 60/2);
	linerel(0,-200);
	moveto(a,a);
	linerel(130,170);
	moveto(a*2,a);
	linerel(-70,170);
	
	outtextxy(300,410,"a");
	outtextxy(200 ,440,strcat(c2,c1));
	
	getch();
	closegraph();
}
